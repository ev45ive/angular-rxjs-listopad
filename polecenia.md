# Git
cd ..
git clone https://bitbucket.org/ev45ive/angular-rxjs-listopad.git angular-rxjs-listopad
cd angular-rxjs-listopad
npm i
npm run start

git stash -u -m "Moje zmiany"
git pull -u origin master
git pull -f 

# Instalacje
node -v
v14.5.0

npm -v
6.14.5

code -v
1.40.1

git --version
git version 2.29.2.windows.2

npm cache clean --force
npm uninstall -g @angular/cli
npm i -g @angular/cli@lastest

npm update @angular/cli@latest -g

ng --version
Angular CLI: 11.0.2

# VS Code Ext
https://marketplace.visualstudio.com/items?itemName=Angular.ng-template
https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode
https://marketplace.visualstudio.com/items?itemName=adrianwilczynski.switcher
https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

# Nowy Project
cd ..
ng new angular-rxjs-listopad
? Do you want to enforce stricter type checking and stricter bundle budgets in the workspace?
  This setting helps improve maintainability and catch bugs ahead of time.
  For more information, see https://angular.io/strict Yes
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS   [ https://sass-lang.com/documentation/syntax#scss]

# Angular Material
ng add @angular/material
? Choose a prebuilt theme name, or "custom" for a custom theme: Indigo/Pink        [ Preview: https://material.angular.io?theme=indigo-pink ]
? Set up global Angular Material typography styles? Yes
? Set up browser animations for Angular Material? Yes

npm run ng -- serve

# App Structure
## LIFT https://angular.io/guide/styleguide#lift

## feature modules https://angular.io/guide/feature-modules
npm run ng -- generate @schematics/angular:module --name=playlists --module=app --route=playlists --routing 
ng generate @schematics/angular:component --name=playlists/components/playlist-list
ng generate @schematics/angular:component --name=playlists/components/playlist-list-item
ng generate @schematics/angular:component --name=playlists/components/playlist-details
ng generate @schematics/angular:component --name=playlists/components/playlist-edit-form

npm run ng -- generate @schematics/angular:module --name=music-search --module=app --route=music  --routing 

## shared module https://angular.io/guide/sharing-ngmodules
npm run ng -- generate @schematics/angular:module --name=shared --module=playlists 
ng generate @schematics/angular:pipe --name=shared/yesno --export --no-flat 

## Core module
ng g m core -m app
ng g i core/model/playlist

## Services
ng g s core/services/playlist --flat false 

## Music search
npm run ng -- generate @schematics/angular:module --name=music-search --module=app --route=music  --routing 
ng g c music-search/components/search-form
ng g c music-search/components/search-results
ng g c music-search/components/album-card

MusicSearch Service
- Inject mock data 
- searchAlbums(query:string):Albums[] - returns mocks

Search Form
- emits query string when 'search' click

Music Search
- searchAlbums on search event

Search results
- display list of album cards with data

# OAuth2.0
ng g s core/services/auth --flat false

npm i angular-oauth2-oidc
import { OAuthModule } from 'angular-oauth2-oidc';
imports:[ OAuthModule.forRoot(), ...


# RxJS
https://gist.github.com/staltz/868e7e9bc2a7b8c1f754

https://rxmarbles.com/
https://rxviz.com/examples/higher-order-observable
https://rxjs.dev/api


# Rxjs error handling
https://medium.com/angular-in-depth/power-of-rxjs-when-using-exponential-backoff-a4b8bde276b0

ng g interceptor core/interceptors/auth
<!-- OR  -->
https://manfredsteyer.github.io/angular-acoauth2-oidc/docs/additional-documentation/working-with-httpinterceptors.html


# NGRX
npm install --save @ngrx/schematics @ngrx/store @ngrx/effects @ngrx/entity @ngrx/store-devtools

ng add @ngrx/schematics@latest
ng add @ngrx/store@latest 

ng generate @ngrx/schematics:store --name=State --module=app --no-flat --root --stateInterface=AppState
ng generate @ngrx/schematics:effect --name=App --module=app --api --group --root
ng generate @ngrx/schematics:action --name=playlist --api --group 

// root
ng generate @ngrx/schematics:reducer --name=playlists --module=app --api --group --reducers=reducers/index.ts

// lazy loaded - feature
ng generate @ngrx/schematics:reducer --name=playlists --module=app --api --group --feature

ng generate @ngrx/schematics:selector --name=playlists --group 

// container
 ng generate @ngrx/schematics:container --name=playlists/containers/playlists-ngrx --module=playlists --changeDetection=OnPush --stateInterface=AppState --no-interactive 

// or entire feature
ng generate @ngrx/schematics:feature --name=music --api --group -m music-search

