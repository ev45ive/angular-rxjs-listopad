import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicSearchRoutingModule } from './music-search-routing.module';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import * as fromMusic from '../reducers/music.reducer';
import { EffectsModule } from '@ngrx/effects';
import { MusicEffects } from '../effects/music.effects';


@NgModule({
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MusicSearchRoutingModule,
    StoreModule.forFeature(fromMusic.musicFeatureKey, fromMusic.reducer),
    EffectsModule.forFeature([MusicEffects])
  ]
})
export class MusicSearchModule { }
