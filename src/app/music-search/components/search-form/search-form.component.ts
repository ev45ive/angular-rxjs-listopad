import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, FormControl, FormGroup, FormGroupDirective, NgForm, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { combineLatest, from, merge, Observable, Observer, scheduled, Subscriber, zip } from 'rxjs';
import { debounceTime, distinctUntilChanged, distinctUntilKeyChanged, filter, map, mapTo, mergeAll, tap, throttle, throttleTime, withLatestFrom } from 'rxjs/operators';
import { AppComponent } from 'src/app/app.component';

// declare global {
//   interface Window {
//     form: FormGroup
//   }
// }
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

const censor: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const badword = 'batman'
  return String(control.value).includes(badword) ? { 'censor': { badword } } : null
  // return { required: true, minlength: { requiredLength: 123 } }
}

const asyncCensor: AsyncValidatorFn = (control: AbstractControl): Observable<ValidationErrors | null> => {
  // return this.http.get('../validate?q='+control.valid).pipe(map(res => {error:..}:null))
  return new Observable((observer: Observer<ValidationErrors | null>) => {
    /* console.log('on subscribe')
 */    const errors = censor(control)

    const handle = setTimeout(() => {
      /* console.log('on validation response' )*/
      observer.next(errors)
      observer.complete()
    }, 1000)

    return () => { /* console.log('on unsubscribe');  */clearTimeout(handle); }
  })
}

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchFormComponent implements OnInit {
  matcher = new MyErrorStateMatcher()

  @Input() set query(q: string | null) {
    if (!q) { return }
    this.searchForm.patchValue({ query: q }, {
      emitEvent: false,
      // onlySelf: true
    })
    this.searchForm.updateValueAndValidity({})
  }

  @Output() search = new EventEmitter<string>();
  private searchClicks = new EventEmitter<void>();

  searchForm = new FormGroup({
    query: new FormControl('', {
      // updateOn:'blur',
      validators: [
        // censor,
        Validators.required,
        Validators.minLength(3),
      ],
      asyncValidators: [asyncCensor]
    }),
    type: new FormControl('album'),
  })

  constructor(private cdk: ChangeDetectorRef) {
    (window as any).form = this.searchForm
    // this.searchForm.valueChanges.subscribe(...)
  }

  ngOnInit(): void {
    const queryField = this.searchForm.get('query')!;
    const valueChanges = queryField!.valueChanges as Observable<string>
    const statusChanges = this.searchForm.get('query')!.statusChanges as Observable<"VALID" | "INVALID" | "PENDING">
    const valid = statusChanges.pipe(filter(s => s == 'VALID'), mapTo(true))

    statusChanges.subscribe(() => {
      setTimeout(() => this.cdk.detectChanges())
    })

    // combineLatest([valid, valueChanges]).pipe(
    //   map(([valid, value]) => value)
    // )

    const inputSearchChanges = valid.pipe(withLatestFrom(valueChanges),
      map(([valid, value]) => value)
    ).pipe(
      debounceTime(400),
      distinctUntilChanged(),
      filter(query => query.length >= 3),
    );

    const buttonSearchClicks = this.searchClicks.pipe(
      filter(() => queryField.valid),
      map(() => queryField.value),
      throttleTime(1000)
    );

    // Subject. next( newObservable )
    from([inputSearchChanges, buttonSearchClicks])
      .pipe(mergeAll())
      .subscribe(this.search)

  }

  submitSearch() {
    this.searchClicks.emit()
    // this.searchClicks.complete()
  }


}
