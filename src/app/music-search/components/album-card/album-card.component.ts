import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Album } from 'src/app/core/model/album';

@Component({
  selector: 'app-album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlbumCardComponent implements OnInit {

  @Input() album!: Album

  constructor() { }

  ngOnInit(): void {
    if (!this.album) {
      throw new Error('MIssing [album] input')
    }
  }

}
