import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpBackend,
  HttpErrorResponse
} from '@angular/common/http';
import { from, Observable, of, throwError } from 'rxjs';

import { OAuthService, AUTH_CONFIG } from 'angular-oauth2-oidc';
import { query } from '@angular/animations';
import { retryWhen, switchMap, delay, mergeMap, catchError } from 'rxjs/operators';
import { Album } from '../model/album';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private oAuth: OAuthService) { }

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler): Observable<HttpEvent<unknown>> {

    // if (!request.url.match('/spotify')) {
    //   return next.handle(request)
    // }

    const authRequest = this.getAuthorizedRequest(request)

    return next.handle(authRequest).pipe(
      retryWhen((errors) => {
        // Lost connections
        return errors.pipe(
          switchMap(err => err.status === 0 ? of(err) : throwError(err)),
          delay(1000),// take(3),
          mergeMap((err, index) => index >= 2 ? throwError(err) : of(err))
        )
      }),
      catchError((error, caughtSource) => {
        // Client error
        if ((!(error instanceof HttpErrorResponse)) || error.status === 0) {
          console.error(error)
          return throwError(new Error('Unexpected error. Try again later'))
        }

        // Auth error
        if (error.status === 401) {
          return from(this.oAuth.initLoginFlowInPopup()).pipe(
            switchMap(() => next.handle(this.getAuthorizedRequest(request))))
        }

        // Server error response
        return throwError(new Error((error.error.error.message)))
      }),
    )
  }

  private getAuthorizedRequest(request: HttpRequest<unknown>) {
    return request.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.oAuth.getAccessToken()
      },
    });
  }
}

// client.get => Handler.request;
// Handler.next = A;
// A.next = B;
// B.next = C
// C.next = HttpBackend
