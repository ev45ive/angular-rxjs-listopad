import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { EventEmitter, Inject, Injectable, Optional } from '@angular/core';
import { BehaviorSubject, EMPTY, from, interval, Observable, observable, of, ReplaySubject, Subject, Subscription, throwError } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { Album, AlbumsSearchResponse } from '../../model/album';
import { API_URL_ROOT, MUSIC_SERVICE_DATA } from '../../tokens';

import { OAuthService, AUTH_CONFIG } from 'angular-oauth2-oidc';
import { catchError, concatAll, concatMap, concatMapTo, delay, exhaust, finalize, map, mergeAll, mergeMap, pluck, retry, retryWhen, startWith, switchAll, switchMap, take, tap } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class MusicSearchService {
  private errorsNotifications = new ReplaySubject<string>(5, 10_000)

  private results = new BehaviorSubject<Album[]>([])
  resultsChanges = this.results.asObservable()

  private queries = new ReplaySubject<string>(5)
  public queryChanges = this.queries.asObservable()

  constructor(
    @Optional() @Inject(MUSIC_SERVICE_DATA) initialResults: Album[],
    private http: HttpClient,
    @Inject(API_URL_ROOT) private api_url: string
  ) {
    this.results.next(initialResults || []);

    (window as any).subject = this.results

    this.queries.pipe(
      switchMap(query => this.requestSearchAlbums(query).pipe(
        catchError((error) => {
          this.errorsNotifications.next(error)
          return EMPTY;
        })
      )),
      /* ... */
      retry()
    ).subscribe(this.results)
  }

  searchAlbums(query: string) {
    this.queries.next(query)
  }

  requestSearchAlbums(query: string) {
    return this.http.get<AlbumsSearchResponse>(`${this.api_url}/v1/search`, {
      params: { type: 'album', q: query }
    }).pipe(
      map(res => res.albums.items)
    );
  }

}
