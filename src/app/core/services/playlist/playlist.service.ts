import { HttpClient } from '@angular/common/http';
import { EventEmitter, Inject, Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { PagingObject } from '../../model/album';
import { Playlist, PlaylistsSource } from '../../model/playlist';
import { PLAYLISTS_DATA } from '../../tokens';

@Injectable({
  // providedIn: CoreModule
  providedIn: 'root'
})
export class PlaylistService implements PlaylistsSource {
  fetchCurrentUserPlaylists() {
    return this.http.get<PagingObject<Playlist>>(`https://api.spotify.com/v1/me/playlists`).pipe(
      map(resp => resp.items)
    )
  }

  playlistsChange = new EventEmitter<Playlist[]>()

  constructor(
    private http: HttpClient,
    @Inject(PLAYLISTS_DATA) private playlists: Playlist[]
  ) { }

  getPlaylistById(selectedId: string | undefined): Playlist | undefined {
    return this.playlists.find(p => p.id === selectedId)
  }

  getPlaylists() {
    return this.playlists
  }

  savePlaylist(draft: Playlist) {
    this.playlists = this.playlists.map(p => p.id === draft.id ? draft : p)

    this.playlistsChange.emit(this.playlists)
  }

}
