import { InjectionToken } from '@angular/core';
import { Album } from './model/album';
import { Playlist } from './model/playlist';

export const PLAYLISTS_DATA = new InjectionToken<Playlist[]>('Intial Data for Playlists Service');

export const MUSIC_SERVICE_DATA = new InjectionToken<Album[]>('Intial Data for Music Service');

export const API_URL_ROOT = new InjectionToken<Album[]>('API Root URL');
