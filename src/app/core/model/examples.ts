export {}

interface Track {
  type: 'track'
  id: string;
  name: string;
  duration: number
}

interface Playlist {
  type: 'playlist' // Discriminatior
  id: string;
  name: string;
  /**
   * Is playlist publicly visible?
   */
  public: boolean;
  description: string;
  tracks?: Track[]
}

// Type Union
const pt: Track | Playlist = {} as any;

// Type Guard
if (pt.type == 'playlist') {
  pt.tracks
} else {
  pt.duration
}


// interface Playlist {
//   tracks?: Track[]
// }
// const p: Playlist = {} as Playlist;

// if (p.tracks) { p.tracks.length }
// const x = p.tracks ? p.tracks.length : 0
// const y = p.tracks && p.tracks.length
// const z = p.tracks?.length
// const areyousure = p.tracks!.length
// areyousure.toFixed()

// p.tracks.push()

// interface Entity {
//   id: string | number;
//   name: string
// }
// const e: Entity = {} as Entity;
// // e.id.toString()

// if ('string' === typeof e.id) {
//   e.id.toUpperCase()
// } else {
//   e.id.toExponential()
// }

// interface Vector { x: number; y: number, length:number }
// interface Point { x: number; y: number }

// let v: Vector = { x: 123, y: 425, length:123 }
// let p: Point = { x: 123, y: 425 }

// // v = p; // Property 'length' is missing in type 'Point' but required in type 'Vector'.
// p = v; // OK
