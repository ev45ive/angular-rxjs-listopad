import { InjectionToken } from '@angular/core';

export interface Playlist {
  id: string;
  name: string;
  /**
   * Is playlist publicly visible?
   */
  public: boolean;
  description: string;
}

export const PlaylistsSource = new InjectionToken('PlaylistsSource Interface')
export interface PlaylistsSource {
  getPlaylists(): Playlist[]
}
