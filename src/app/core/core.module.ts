import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { API_URL_ROOT, PLAYLISTS_DATA } from './tokens';
import { PlaylistsSource } from './model/playlist';
import { PlaylistService } from './services/playlist/playlist.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { OAuthModule, AUTH_CONFIG } from 'angular-oauth2-oidc';
import { AuthService } from './services/auth/auth.service';
import { AuthInterceptor } from './interceptors/auth.interceptor';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    OAuthModule.forRoot()
  ],
  providers: [
    // {
    //   provide: HttpClient,
    //   useClass MyAwesomeAuthHttpClient
    // },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true // collect intercetors into Array[]
    },
    {
      provide: AUTH_CONFIG,
      useValue: environment.authImplicitFlowConfig
    },
    {
      provide: PLAYLISTS_DATA,
      useValue: []
    },
    {
      provide: API_URL_ROOT,
      useValue: environment.api_url
    },
    // {
    //   // provide: PlaylistService, // by Class
    //   provide: PlaylistsSource, // by 'Interface' (token)
    //   useFactory(data:Playlist[]/* valueB, serviceC */) {
    //     return new PlaylistService(data)
    //   },
    //   deps: [PLAYLISTS_DATA/*, tokenB, tokenC  */]
    // },
    // {
    //   provide: PlaylistsSource,
    //   useClass: PlaylistService,
    //   // deps: [PLAYLISTS_DATA] // automatic from @Inject(...)
    // },
    // {
    //   provide: PlaylistService,
    //   useClass: PlaylistService
    // },
    // PlaylistService,
    {
      provide: PlaylistsSource,
      useExisting: PlaylistService
    }
  ]
})
export class CoreModule {
  constructor(
    @Optional() @SkipSelf() private parent: CoreModule,
    private auth: AuthService
  ) {
    if (parent !== null) {
      throw new Error('Core module should be importent only once in root module!')
    }

    this.auth.init()
  }
}
