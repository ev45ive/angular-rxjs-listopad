import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { playlistsMock } from './core/data/playlists.mock';
import { MUSIC_SERVICE_DATA, PLAYLISTS_DATA } from './core/tokens';
import { albumsMock } from './core/data/albums.mock';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './effects/app.effects';
import * as fromPlaylists from './reducers/playlists.reducer';
import { PlaylistsEffects } from './effects/playlists.effects';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    // SuperHiperUltraWidget
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    BrowserAnimationsModule,
    CoreModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([AppEffects]),
    EffectsModule.forFeature([PlaylistsEffects]),
  ],
  providers: [
    {
      provide: PLAYLISTS_DATA,
      useValue: playlistsMock
    },
    {
      provide: MUSIC_SERVICE_DATA,
      useValue: albumsMock
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
