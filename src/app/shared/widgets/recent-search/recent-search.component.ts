import { Component, OnInit } from '@angular/core';
import { tap } from 'rxjs/operators';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';

@Component({
  selector: 'app-recent-search',
  templateUrl: './recent-search.component.html',
  styleUrls: ['./recent-search.component.scss']
})
export class RecentSearchComponent implements OnInit {

  recent: string[] = []

  constructor(
    private service: MusicSearchService
  ) { }

  ngOnInit(): void {
    this.service.queryChanges.pipe(
      tap(console.log)
    )
      .subscribe((query: string) => {
        this.addNewSearch(query)
      })
  }

  search(query: string) {
    this.service.searchAlbums(query)
  }

  addNewSearch(query: string) {
    this.recent.unshift(query)
    this.recent = this.recent.slice(0, 5)
  }

}
