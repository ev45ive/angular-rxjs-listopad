import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromPlaylists from './playlists.reducer';


export interface AppState {

  [fromPlaylists.playlistsFeatureKey]: fromPlaylists.State;
}

export const reducers: ActionReducerMap<AppState> = {

  [fromPlaylists.playlistsFeatureKey]: fromPlaylists.reducer,
};


export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
