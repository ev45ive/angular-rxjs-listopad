import { Action, createReducer, on } from '@ngrx/store';
import * as MusicActions from '../actions/music.actions';

export const musicFeatureKey = 'music';

export interface State {

}

export const initialState: State = {

};


export const reducer = createReducer(
  initialState,

  on(MusicActions.loadMusics, state => state),
  on(MusicActions.loadMusicsSuccess, (state, action) => state),
  on(MusicActions.loadMusicsFailure, (state, action) => state),

);

