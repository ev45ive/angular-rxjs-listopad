import { Action, createReducer, on } from '@ngrx/store';
import * as PlaylistsActions from '../actions/playlist.actions';
import { Playlist } from '../core/model/playlist';

export const playlistsFeatureKey = 'playlists';

export interface State {
  items: Playlist[],
  loading: boolean,
  message: string,
  selectedId?: Playlist['id']
}

export const initialState: State = {
  items: [],
  loading: false,
  message: ''
};


export const reducer = createReducer(
  initialState,

  on(PlaylistsActions.loadPlaylists, state => ({
    ...state, loading: true, message: ''
  })),
  on(PlaylistsActions.loadPlaylistsSuccess, (state, action) => ({
    ...state, loading: false, items: action.data
  })),
  on(PlaylistsActions.loadPlaylistsFailure, (state, action) => ({
    ...state, loading: false, message: action.error?.message || 'Unexpected error'
  })),
  on(PlaylistsActions.selectPlaylist, (state, action) => ({
    ...state, selectedId: action.id
  })),
);

