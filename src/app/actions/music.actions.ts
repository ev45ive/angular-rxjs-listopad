import { createAction, props } from '@ngrx/store';

export const loadMusics = createAction(
  '[Music] Load Musics'
);

export const loadMusicsSuccess = createAction(
  '[Music] Load Musics Success',
  props<{ data: any }>()
);

export const loadMusicsFailure = createAction(
  '[Music] Load Musics Failure',
  props<{ error: any }>()
);
