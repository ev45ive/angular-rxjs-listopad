import * as fromMusic from './music.actions';

describe('loadMusics', () => {
  it('should return an action', () => {
    expect(fromMusic.loadMusics().type).toBe('[Music] Load Musics');
  });
});
