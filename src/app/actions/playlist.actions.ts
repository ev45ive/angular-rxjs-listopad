import { createAction, props } from '@ngrx/store';
import { Playlist } from '../core/model/playlist';

export const loadPlaylists = createAction(
  '[Playlist] Load Playlists'
);

export const loadPlaylistsSuccess = createAction(
  '[Playlist] Load Playlists Success',
  props<{ data: Playlist[] }>()
);

export const loadPlaylistsFailure = createAction(
  '[Playlist] Load Playlists Failure',
  props<{ error: any }>()
);

export const selectPlaylist = createAction(
  '[Playlist] selectPlaylist',
  props<{ id: Playlist['id'] }>()
);
