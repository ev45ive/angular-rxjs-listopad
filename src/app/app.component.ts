import { Component } from '@angular/core';
import { OAuthService, AUTH_CONFIG } from 'angular-oauth2-oidc';
import { AuthService } from './core/services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-rxjs-listopad';
  hideRecent = false
  menuOpen = false

  login(){
    this.oAuth.login()
  }

  constructor(private oAuth: AuthService) { }
}
