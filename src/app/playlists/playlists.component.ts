import { Component, Inject, OnInit } from '@angular/core';
import { Playlist, PlaylistsSource } from '../core/model/playlist';
import { PlaylistService } from '../core/services/playlist/playlist.service';

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss']
})
export class PlaylistsComponent implements OnInit {

  mode: 'details' | 'edit' = 'details';

  playlists: Playlist[] = []

  selectedId?: Playlist['id']
  selected?: Playlist

  constructor(
    private playlistService: PlaylistService
  ) { }

  ngOnInit(): void {
    this.playlists = this.playlistService.getPlaylists()
    this.playlistService.playlistsChange
      .subscribe((playlists: Playlist[]) => {
        this.playlists = playlists
        this.selected = this.playlistService.getPlaylistById(this.selectedId)
      })
  }


  cancel() { }
  save(draft: Playlist) {
    this.playlistService.savePlaylist(draft)
    this.switchToDetails()
  }

  select(id: Playlist['id']) {
    this.selectedId = this.selectedId === id ? undefined : id;
    this.selected = this.playlistService.getPlaylistById(this.selectedId)
  }

  switchToEdit() {
    this.mode = 'edit'
  }
  switchToDetails() {
    this.mode = 'details'
  }


}
