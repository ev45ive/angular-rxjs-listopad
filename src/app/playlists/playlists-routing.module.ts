import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlaylistsNgrxComponent } from './containers/playlists-ngrx/playlists-ngrx.component';


const routes: Routes = [{ path: '', component: PlaylistsNgrxComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule { }
