import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { loadPlaylists, selectPlaylist } from 'src/app/actions/playlist.actions';
import { selectPlaylistsItems, selectSelectedPlaylist } from 'src/app/selectors/playlists.selectors';

@Component({
  selector: 'app-playlists-ngrx',
  templateUrl: './playlists-ngrx.component.html',
  styleUrls: ['./playlists-ngrx.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistsNgrxComponent implements OnInit {

  playlists = this.store.select(selectPlaylistsItems)
  selected = this.store.select(selectSelectedPlaylist)

  constructor(private store: Store) { }

  selectById(id: string) {
    this.store.dispatch(selectPlaylist({ id }))
  }

  ngOnInit(): void {
    this.store.dispatch(loadPlaylists())
  }

}
