import { NgForOf, NgForOfContext } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

NgForOf
NgForOfContext

@Component({
  selector: 'app-playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
  // inputs: ['playlists:items']
})
export class PlaylistListComponent implements OnInit {

  @Input('items') playlists?: Playlist[] = []

  @Input() selectedId?: Playlist['id']

  @Output() selectedIdChange = new EventEmitter<Playlist['id']>();

  // constructor(private parent: PlaylistsComponent) { }

  select(id: Playlist['id']) {
    this.selectedIdChange.emit(id)
    // this.selectedId = id;
    // this.parent.select(id)
  }

  trackFn(index: number, item: any) {
    return item.id
  }


  ngOnInit(): void {
  }

}
