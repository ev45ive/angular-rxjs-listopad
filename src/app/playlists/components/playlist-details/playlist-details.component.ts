import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistDetailsComponent implements OnInit {

  @Input() playlist!:Playlist

  @Output() edit = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    if(!this.playlist){
      throw new Error('[playlist] input is required')
    }
  }

}
