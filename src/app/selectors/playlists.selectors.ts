import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromPlaylists from '../reducers/playlists.reducer';

const featureSelector = createFeatureSelector<fromPlaylists.State>(fromPlaylists.playlistsFeatureKey)

export const selectPlaylistsItems = createSelector(
  featureSelector,
  state => state.items
)

export const selectedId = createSelector(featureSelector, state => state.selectedId)

export const selectSelectedPlaylist = createSelector(
  selectPlaylistsItems,
  selectedId,
  (items, id) => items.find(p => p.id === id)
)
