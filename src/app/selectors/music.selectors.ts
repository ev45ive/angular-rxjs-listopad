import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromMusic from '../reducers/music.reducer';

export const selectMusicState = createFeatureSelector<fromMusic.State>(
  fromMusic.musicFeatureKey
);
