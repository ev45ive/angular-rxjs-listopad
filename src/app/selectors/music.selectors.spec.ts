import * as fromMusic from '../reducers/music.reducer';
import { selectMusicState } from './music.selectors';

describe('Music Selectors', () => {
  it('should select the feature state', () => {
    const result = selectMusicState({
      [fromMusic.musicFeatureKey]: {}
    });

    expect(result).toEqual({});
  });
});
