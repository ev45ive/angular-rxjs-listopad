import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { EMPTY, from, Observable } from 'rxjs';
import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';
import * as PlaylistsActions from '../actions/playlist.actions';
import { PlaylistService } from '../core/services/playlist/playlist.service';



@Injectable()
export class PlaylistsEffects {

  constructor(

    private playlistsService: PlaylistService,
    private actions$: Actions) { }

  loadPlaylists = createEffect(() => this.actions$.pipe(
    ofType(
      PlaylistsActions.loadPlaylists/* ,
      PlaylistsActions.loadPlaylists,
      PlaylistsActions.loadPlaylists */),
    switchMap(action => {

      return this.playlistsService.fetchCurrentUserPlaylists().pipe(
        mergeMap(resp => [
          PlaylistsActions.loadPlaylistsSuccess({ data: resp })
        ]),
        catchError(err => [
          PlaylistsActions.loadPlaylistsFailure({ error: err })
        ])
      )

      // return from([]) as Observable<Action>
    })
  ))

}
