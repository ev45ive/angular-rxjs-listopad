import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { MusicEffects } from './music.effects';

describe('MusicEffects', () => {
  let actions$: Observable<any>;
  let effects: MusicEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MusicEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.inject(MusicEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
