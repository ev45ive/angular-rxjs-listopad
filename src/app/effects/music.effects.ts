import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, concatMap } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';

import * as MusicActions from '../actions/music.actions';



@Injectable()
export class MusicEffects {

  loadMusics$ = createEffect(() => {
    return this.actions$.pipe( 

      ofType(MusicActions.loadMusics),
      concatMap(() =>
        /** An EMPTY observable only emits completion. Replace with your own observable API request */
        EMPTY.pipe(
          map(data => MusicActions.loadMusicsSuccess({ data })),
          catchError(error => of(MusicActions.loadMusicsFailure({ error }))))
      )
    );
  });



  constructor(private actions$: Actions) {}

}
